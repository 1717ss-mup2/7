#!/usr/bin/env clisp

;author Leonard Krause

(defun fakultat (n)
	(if (< n 2)
		1
		(* n (fakultat (- n 1)))
		)
	)

(defun binom (n k)
	(/ (fakultat n) (* (fakultat k) (fakultat (- n k))))
	)

; Testen Sie beide Methoden mit jeweils mindestens zwei Beispielen
(format t "~%~%2!= ")
(print (fakultat 2))

(format t "~%~%100!= ")
(print (fakultat 100))

(format t "~%~%4 ueber 2= ")
(print (binom 2 4))
(format t "~%~%1000 ueber 999= ")
(print (binom 1000 999))
(format t "~%~%5682 ueber 999= ")
(print (binom 5682 10))

;Eingabe beliebiger Werte
(format t "~%~%Eingabe a,b fuer a ueber b: ")
(format t "~%Eingabe a: ")
(setq a (read))
(format t "Eingabe b: ")
(setq b (read))
(format t "a ueber b=")
(print (binom a b))

; Für welche a arbeitet Ihr Programm, bei gültigem b, noch korrekt?

; >Für alle a<=6582, danach kommt es zu einem Stack-Overflow.


; Was können Sie entsprechend über die Rechengenauigkeit sagen
; und vergleichen Sie dies insbesondere mit dem Wertebereich von
; vorzeichenlosen Ganzzahlen in 64Bit. Geben Sie bitte Ihre Antwort in Kommentarform ab.

; >Die Rechengenauigkeit ist extrem gut, da es in Lisp kein Limit für die Größe von
; >Integern gibt. (wird nur durch den verfügbaren Speicher limitiert)
; >[siehe Integer](http://clhs.lisp.se/Body/t_intege.htm#integer)
; >eine vorzeichenlose 64 bit Ganzahl kann dagegen nur den maximalen Wert 18.446.744.073.709.551.615 annehmen.
; >Jeder moderne Computer hat jedoch mehr als 64 Bit Ram zu verfügung :)
; >Die Rechengenauigkeit wird in Lisp also eig nur durch die maximale Größe des Stacks limitiert.