#!/usr/bin/env clisp

;author Leonard Krause

(defun enhancedString ()
               (make-array 0
                           :fill-pointer 0
                           :adjustable t
                           :element-type 'character
                )
    )

(defun add (a b limitLow limitHigh)
	(if (eq b 0)
		a
		(if (< a limitHigh)
			(add (+ a 1) (- b 1) limitLow limitHigh)
			(add limitLow (- b 1) limitLow limitHigh)
			)
		)
	)

(defun encode (c key)
	(setq charCode (char-code c))
	;uppercase
	(if (and (>= charCode 65) (<= charCode 90))
		(setq c (code-char (add charCode key 65 90)))
		)
	;lowercase
	(if (and (>= charCode 97) (<= charCode 122))
		(setq c (code-char (add charCode key 97 122)))
		)
	c
	)

(defun caesarStringInit (text key)
	(setq code (enhancedString))
	(caesarString text key code)
	)

(defun caesarString (text key code)
	(if (eq (length text) 0)
		code
		(progn 
			(setq c (encode (char text 0) key))
			(vector-push-extend c code)
			(caesarString (subseq text 1) key code)
			)
		)
	)

(format t "Beispiel (caesarString key text) key=13 text=\"Hello World\"")
(setq key 13)
(setq text "Hello World!")
(print (caesarStringInit text key))

(defun caesarConsole (key)
	(setq code (enhancedString))
	(loop
		(setq c (read-char))
		(if (eq c #\$)
			(return code)
			(vector-push-extend (encode c key) code)
			)
		)
	)

(format t "~%~%Beispiel (caesarConsole key) (reads Characters from Console)")
(format t "~%Eingabe key (default=13, wenn eingabe keine Ganzzahl):")
(setq key (read))
(if (not (typep key 'integer)) (setq key 13))
(format t "~%String eingeben (Ende mit $): ~%")
(print (caesarConsole key))